<?xml version="1.0" encoding="UTF-8"?>
<solution name="spaceship" uuid="01652d0f-c66f-4eb2-bdd6-97e5e98b2c17" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">~_PreferencesModule#tutorial.__spreferences.PlatformTemplates(tutorial.__spreferences.PlatformTemplates)</dependency>
    <dependency reexport="false">2ed50273-af07-4e30-9004-b1f89545178a(com.mbeddr.core.stdlib)</dependency>
  </dependencies>
  <languageVersions />
  <dependencyVersions>
    <module reference="01652d0f-c66f-4eb2-bdd6-97e5e98b2c17(spaceship)" version="0" />
  </dependencyVersions>
</solution>

