<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:bc624fbc-9b80-41df-8da9-e9315e2ab76c(spaceship.spaceship)">
  <persistence version="9" />
  <languages>
    <devkit ref="1a986be1-0ef0-4f9f-9d8a-81c3ea7227ae(com.mbeddr.physicalunits)" />
    <devkit ref="d2a9c55c-6bdc-4cc2-97e1-4ba7552f5584(com.mbeddr.core)" />
    <devkit ref="43d889ae-8e6a-4f6e-a649-d59342d8728d(com.mbeddr.statemachines)" />
  </languages>
  <imports>
    <import index="mfo1" ref="r:14605b73-d8b0-4e4f-8b1f-a4521795ebbf(tutorial.__spreferences.PlatformTemplates)" />
    <import index="3y0n" ref="r:d4d16117-20fb-4ba8-a1b2-1598e121e1d0(com.mbeddr.core.stdlib)" />
  </imports>
  <registry>
    <language id="0d04a6cc-773e-4069-b9b0-11884b2ff1c8" name="com.mbeddr.ext.units">
      <concept id="5348704582971040037" name="com.mbeddr.ext.units.structure.UnitConfigItem" flags="ng" index="2eh4Hv" />
      <concept id="624957442818070507" name="com.mbeddr.ext.units.structure.StripUnitExpression" flags="ng" index="2yh1Mg">
        <child id="624957442818070508" name="innerExpression" index="2yh1Mn" />
      </concept>
      <concept id="8337440621611212272" name="com.mbeddr.ext.units.structure.AnnotatedExpression" flags="ng" index="CIdvy">
        <child id="8337440621611267898" name="innerExpression" index="CIrOC" />
        <child id="8337440621611353453" name="specification" index="CIwXZ" />
      </concept>
      <concept id="8337440621611267903" name="com.mbeddr.ext.units.structure.Unit" flags="ng" index="CIrOH">
        <property id="8337440621611269512" name="description" index="CIruq" />
        <child id="8337440621611270427" name="spec" index="CIsG9" />
      </concept>
      <concept id="8337440621611267900" name="com.mbeddr.ext.units.structure.UnitContainer" flags="ng" index="CIrOI">
        <child id="8337440621611267904" name="contents" index="CIrPi" />
      </concept>
      <concept id="8337440621611273670" name="com.mbeddr.ext.units.structure.IntegerExponent" flags="ng" index="CIsvk">
        <property id="8337440621611273671" name="value" index="CIsvl" />
      </concept>
      <concept id="8337440621611273669" name="com.mbeddr.ext.units.structure.UnitReference" flags="ng" index="CIsvn">
        <reference id="8337440621611297532" name="unit" index="CIi3I" />
        <child id="8337440621611297534" name="exponent" index="CIi3G" />
      </concept>
      <concept id="8337440621611270429" name="com.mbeddr.ext.units.structure.UnitSpecification" flags="ng" index="CIsGf">
        <child id="8337440621611297539" name="components" index="CIi4h" />
      </concept>
      <concept id="8337440621611400980" name="com.mbeddr.ext.units.structure.AnnotatedType" flags="ng" index="CIVk6">
        <child id="8337440621611401032" name="specification" index="CIVlq" />
      </concept>
      <concept id="6111466015651074424" name="com.mbeddr.ext.units.structure.EmptyUnitContainerContent" flags="ng" index="134lye" />
    </language>
    <language id="a9d69647-0840-491e-bf39-2eb0805d2011" name="com.mbeddr.core.statements">
      <concept id="6275792049641600983" name="com.mbeddr.core.statements.structure.IfStatement" flags="ng" index="c0U19">
        <child id="6275792049641600984" name="condition" index="c0U16" />
        <child id="6275792049641600985" name="thenPart" index="c0U17" />
        <child id="3134547887598486571" name="elsePart" index="ggAap" />
      </concept>
      <concept id="7763322639126652757" name="com.mbeddr.core.statements.structure.ITypeContainingType" flags="ng" index="2umbIr">
        <child id="7763322639126652758" name="baseType" index="2umbIo" />
      </concept>
      <concept id="1494329074535282918" name="com.mbeddr.core.statements.structure.ElsePart" flags="ng" index="1ly_i6">
        <child id="1494329074535283249" name="body" index="1ly_ph" />
      </concept>
      <concept id="7254843406768833938" name="com.mbeddr.core.statements.structure.ExpressionStatement" flags="ng" index="1_9egQ">
        <child id="7254843406768833939" name="expr" index="1_9egR" />
      </concept>
      <concept id="1679452829930336984" name="com.mbeddr.core.statements.structure.CommentStatement" flags="ng" index="1QiMYF">
        <child id="8624890525768479139" name="textblock" index="3SJzmv" />
      </concept>
      <concept id="4185783222026475238" name="com.mbeddr.core.statements.structure.LocalVariableDeclaration" flags="ng" index="3XIRlf">
        <child id="4185783222026502647" name="init" index="3XIe9u" />
      </concept>
      <concept id="4185783222026475861" name="com.mbeddr.core.statements.structure.StatementList" flags="ng" index="3XIRFW">
        <child id="4185783222026475862" name="statements" index="3XIRFZ" />
      </concept>
      <concept id="4185783222026464515" name="com.mbeddr.core.statements.structure.Statement" flags="ng" index="3XISUE" />
      <concept id="2093108837558113914" name="com.mbeddr.core.statements.structure.LocalVarRef" flags="ng" index="3ZVu4v">
        <reference id="2093108837558124071" name="var" index="3ZVs_2" />
      </concept>
    </language>
    <language id="92d2ea16-5a42-4fdf-a676-c7604efe3504" name="de.slisson.mps.richtext">
      <concept id="2557074442922380897" name="de.slisson.mps.richtext.structure.Text" flags="ng" index="19SGf9">
        <child id="2557074442922392302" name="words" index="19SJt6" />
      </concept>
      <concept id="2557074442922438156" name="de.slisson.mps.richtext.structure.Word" flags="ng" index="19SUe$">
        <property id="2557074442922438158" name="escapedValue" index="19SUeA" />
      </concept>
    </language>
    <language id="2d7fadf5-33f6-4e80-a78f-0f739add2bde" name="com.mbeddr.core.buildconfig">
      <concept id="5046689135693761556" name="com.mbeddr.core.buildconfig.structure.Binary" flags="ng" index="2eOfOj">
        <reference id="2504745233808502246" name="target" index="3oK8_y" />
        <child id="5046689135693761559" name="referencedModules" index="2eOfOg" />
      </concept>
      <concept id="5046689135693761554" name="com.mbeddr.core.buildconfig.structure.Executable" flags="ng" index="2eOfOl">
        <property id="3431613015799084476" name="isTest" index="iO3LB" />
      </concept>
      <concept id="7717755763392524104" name="com.mbeddr.core.buildconfig.structure.BuildConfiguration" flags="ng" index="2v9HqL">
        <child id="5046689135694070731" name="binaries" index="2ePNbc" />
        <child id="5323740605968447026" name="target" index="2AWWZH" />
      </concept>
      <concept id="7717755763392524107" name="com.mbeddr.core.buildconfig.structure.ModuleRef" flags="ng" index="2v9HqM">
        <reference id="7717755763392524108" name="module" index="2v9HqP" />
      </concept>
      <concept id="5323740605968447022" name="com.mbeddr.core.buildconfig.structure.DesktopPlatform" flags="ng" index="2AWWZL">
        <property id="5323740605968447025" name="compilerOptions" index="2AWWZI" />
        <property id="5323740605968447024" name="compiler" index="2AWWZJ" />
        <property id="3963667026125442601" name="gdb" index="3r8Kw1" />
        <property id="3963667026125442676" name="make" index="3r8Kxs" />
      </concept>
      <concept id="5323740605968447019" name="com.mbeddr.core.buildconfig.structure.Platform" flags="ng" index="2AWWZO">
        <property id="5952395988556102274" name="supportsSharedLibraries" index="uKT8v" />
        <child id="1485382076185232212" name="targets" index="3anu1O" />
      </concept>
      <concept id="1485382076184236780" name="com.mbeddr.core.buildconfig.structure.Target" flags="ng" index="3abb7c" />
      <concept id="2736179788492003936" name="com.mbeddr.core.buildconfig.structure.IDebuggablePlatform" flags="ng" index="1FkSt_">
        <property id="2736179788492003937" name="debugOptions" index="1FkSt$" />
      </concept>
    </language>
    <language id="3bf5377a-e904-4ded-9754-5a516023bfaa" name="com.mbeddr.core.pointers">
      <concept id="6113173064526131575" name="com.mbeddr.core.pointers.structure.StringLiteral" flags="ng" index="PhEJO">
        <property id="6113173064526131578" name="value" index="PhEJT" />
      </concept>
      <concept id="6113173064528067332" name="com.mbeddr.core.pointers.structure.StringType" flags="ng" index="Pu267" />
      <concept id="5679441017214012545" name="com.mbeddr.core.pointers.structure.ArrayType" flags="ng" index="3J0A42">
        <child id="1452920870317474611" name="sizeExpr" index="1YbSNA" />
      </concept>
    </language>
    <language id="2693fc71-9b0e-4b05-ab13-f57227d675f2" name="com.mbeddr.core.util">
      <concept id="4459718605982051949" name="com.mbeddr.core.util.structure.ReportingConfiguration" flags="ng" index="2Q9Fgs">
        <child id="4459718605982051999" name="strategy" index="2Q9FjI" />
      </concept>
      <concept id="4459718605982051980" name="com.mbeddr.core.util.structure.PrintfReportingStrategy" flags="ng" index="2Q9FjX" />
    </language>
    <language id="d4280a54-f6df-4383-aa41-d1b2bffa7eb1" name="com.mbeddr.core.base">
      <concept id="8375407818529178006" name="com.mbeddr.core.base.structure.TextBlock" flags="ng" index="OjmMv">
        <child id="8375407818529178007" name="text" index="OjmMu" />
      </concept>
      <concept id="4459718605982007337" name="com.mbeddr.core.base.structure.IConfigurationContainer" flags="ng" index="2Q9xDo">
        <child id="4459718605982007338" name="configurationItems" index="2Q9xDr" />
      </concept>
      <concept id="3857533489766146428" name="com.mbeddr.core.base.structure.ElementDocumentation" flags="ng" index="1z9TsT">
        <child id="4052432714772608243" name="text" index="1w35rA" />
      </concept>
      <concept id="747084250476811597" name="com.mbeddr.core.base.structure.DefaultGenericChunkDependency" flags="ng" index="3GEVxB">
        <reference id="747084250476878887" name="chunk" index="3GEb4d" />
      </concept>
    </language>
    <language id="6d11763d-483d-4b2b-8efc-09336c1b0001" name="com.mbeddr.core.modules">
      <concept id="3788988821852026523" name="com.mbeddr.core.modules.structure.GlobalConstantRef" flags="ng" index="4ZOvp">
        <reference id="3376775282622611130" name="constant" index="2DPCA0" />
      </concept>
      <concept id="1028666136487545270" name="com.mbeddr.core.modules.structure.CommentModuleContent" flags="ng" index="2B_Gvg">
        <child id="1028666136487550078" name="text" index="2B_H8o" />
      </concept>
      <concept id="8967919205527146149" name="com.mbeddr.core.modules.structure.ReturnStatement" flags="ng" index="2BFjQ_">
        <child id="8967919205527146150" name="expression" index="2BFjQA" />
      </concept>
      <concept id="8105003328814797298" name="com.mbeddr.core.modules.structure.IFunctionLike" flags="ng" index="2H9T1B">
        <child id="5708867820623310661" name="arguments" index="1UOdpc" />
      </concept>
      <concept id="6437088627575722813" name="com.mbeddr.core.modules.structure.Module" flags="ng" index="N3F4X">
        <child id="6437088627575722833" name="contents" index="N3F5h" />
        <child id="1317894735999304826" name="imports" index="2OODSX" />
      </concept>
      <concept id="6437088627575722830" name="com.mbeddr.core.modules.structure.ImplementationModule" flags="ng" index="N3F5e" />
      <concept id="6437088627575722831" name="com.mbeddr.core.modules.structure.IModuleContent" flags="ng" index="N3F5f">
        <property id="1317894735999272944" name="exported" index="2OOxQR" />
      </concept>
      <concept id="6437088627575724001" name="com.mbeddr.core.modules.structure.Function" flags="ng" index="N3Fnx">
        <child id="4185783222026475860" name="body" index="3XIRFX" />
      </concept>
      <concept id="8934095934011938595" name="com.mbeddr.core.modules.structure.EmptyModuleContent" flags="ng" index="2NXPZ9" />
      <concept id="7892328519581704407" name="com.mbeddr.core.modules.structure.Argument" flags="ng" index="19RgSI" />
      <concept id="5950410542643524492" name="com.mbeddr.core.modules.structure.FunctionCall" flags="ng" index="3O_q_g">
        <reference id="5950410542643524493" name="function" index="3O_q_h" />
        <child id="5950410542643524495" name="actuals" index="3O_q_j" />
      </concept>
      <concept id="2093108837558505658" name="com.mbeddr.core.modules.structure.ArgumentRef" flags="ng" index="3ZUYvv">
        <reference id="2093108837558505659" name="arg" index="3ZUYvu" />
      </concept>
    </language>
    <language id="564e97d6-8fb7-41f5-bfc1-c7ed376efd62" name="com.mbeddr.ext.statemachines">
      <concept id="4643433264760980253" name="com.mbeddr.ext.statemachines.structure.InEvent" flags="ng" index="2cfOFI" />
      <concept id="8927638623067326788" name="com.mbeddr.ext.statemachines.structure.EmptyStatemachineContent" flags="ng" index="2h6h52" />
      <concept id="6118219496725500902" name="com.mbeddr.ext.statemachines.structure.SmTriggerTarget" flags="ng" index="$QhJh">
        <reference id="6118219496725502924" name="event" index="$QhfV" />
        <child id="6118219496725502916" name="args" index="$QhfN" />
      </concept>
      <concept id="4753668641245811355" name="com.mbeddr.ext.statemachines.structure.EmptyStateContents" flags="ng" index="ODFVE" />
      <concept id="6118219496719522740" name="com.mbeddr.ext.statemachines.structure.SmInitTarget" flags="ng" index="Vf_e3" />
      <concept id="1786180596061258962" name="com.mbeddr.ext.statemachines.structure.EventArgRef" flags="ng" index="3498Or">
        <reference id="1786180596061258963" name="arg" index="3498Oq" />
      </concept>
      <concept id="1786180596061248885" name="com.mbeddr.ext.statemachines.structure.EventArg" flags="ng" index="349diW" />
      <concept id="1786180596061233739" name="com.mbeddr.ext.statemachines.structure.Trigger" flags="ng" index="349iI2">
        <reference id="8951398808641876049" name="event" index="1bNv6r" />
      </concept>
      <concept id="1786180596061219795" name="com.mbeddr.ext.statemachines.structure.Event" flags="ng" index="349m8q">
        <child id="1786180596061248896" name="args" index="349dh9" />
      </concept>
      <concept id="1786180596061383227" name="com.mbeddr.ext.statemachines.structure.StatemachineVarRef" flags="ng" index="349IfM">
        <reference id="1786180596061383228" name="var" index="349IfP" />
      </concept>
      <concept id="7851711690674263345" name="com.mbeddr.ext.statemachines.structure.StatemachineType" flags="ng" index="3lBjsv">
        <reference id="7851711690674263346" name="machine" index="3lBjss" />
      </concept>
      <concept id="4709703140582114943" name="com.mbeddr.ext.statemachines.structure.StatemachineConfigItem" flags="ng" index="3yF7LM">
        <property id="4709703140582114945" name="triggerAsConst" index="3yF7Mc" />
      </concept>
      <concept id="1270667558200936379" name="com.mbeddr.ext.statemachines.structure.AbstractTransition" flags="ng" index="1zz5ri">
        <reference id="1270667558201034238" name="targetState" index="1zztin" />
        <child id="1270667558200943847" name="guard" index="1zz7me" />
        <child id="1270667558200946447" name="actions" index="1zz7TA" />
      </concept>
      <concept id="4249345261280334498" name="com.mbeddr.ext.statemachines.structure.AbstractState" flags="ng" index="1Koyuy">
        <child id="4249345261280348989" name="contents" index="1KoBSX" />
      </concept>
      <concept id="5778488248013533809" name="com.mbeddr.ext.statemachines.structure.Statemachine" flags="ng" index="1LFe83">
        <reference id="5778488248013533842" name="initial" index="1LFebw" />
        <child id="7835233251114737454" name="contents" index="1_Iowf" />
      </concept>
      <concept id="5778488248013533883" name="com.mbeddr.ext.statemachines.structure.Transition" flags="ng" index="1LFeb9">
        <child id="3670856444174351950" name="trigger" index="2qxFSM" />
      </concept>
      <concept id="5778488248013533839" name="com.mbeddr.ext.statemachines.structure.State" flags="ng" index="1LFebX" />
      <concept id="5633981208992643165" name="com.mbeddr.ext.statemachines.structure.StatemachineVariableDeclaration" flags="ng" index="1R59hi">
        <child id="4643433264760912612" name="init" index="2cfFcn" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
    <language id="61c69711-ed61-4850-81d9-7714ff227fb0" name="com.mbeddr.core.expressions">
      <concept id="8463282783691618456" name="com.mbeddr.core.expressions.structure.UnsignedInt64tType" flags="ng" index="26Vqp1" />
      <concept id="8463282783691618461" name="com.mbeddr.core.expressions.structure.UnsignedInt8tType" flags="ng" index="26Vqp4" />
      <concept id="8463282783691618450" name="com.mbeddr.core.expressions.structure.UnsignedInt32tType" flags="ng" index="26Vqpb" />
      <concept id="8463282783691618440" name="com.mbeddr.core.expressions.structure.Int32tType" flags="ng" index="26Vqph" />
      <concept id="8463282783691618445" name="com.mbeddr.core.expressions.structure.Int64tType" flags="ng" index="26Vqpk" />
      <concept id="8463282783691618435" name="com.mbeddr.core.expressions.structure.Int16tType" flags="ng" index="26Vqpq" />
      <concept id="8463282783691618466" name="com.mbeddr.core.expressions.structure.UnsignedInt16tType" flags="ng" index="26VqpV" />
      <concept id="8463282783691618471" name="com.mbeddr.core.expressions.structure.UnsignedLongLongType" flags="ng" index="26VqpY" />
      <concept id="8463282783691618426" name="com.mbeddr.core.expressions.structure.Int8tType" flags="ng" index="26Vqqz" />
      <concept id="8463282783691596316" name="com.mbeddr.core.expressions.structure.LongLongType" flags="ng" index="26VBN5" />
      <concept id="8463282783691596310" name="com.mbeddr.core.expressions.structure.UnsignedLongType" flags="ng" index="26VBNf" />
      <concept id="8463282783691492730" name="com.mbeddr.core.expressions.structure.UnsignedIntType" flags="ng" index="26VXez" />
      <concept id="8463282783691492716" name="com.mbeddr.core.expressions.structure.UnsignedCharType" flags="ng" index="26VXeP" />
      <concept id="3005510381523579442" name="com.mbeddr.core.expressions.structure.UnaryExpression" flags="ng" index="2aKSnQ">
        <child id="7254843406768839760" name="expression" index="1_9fRO" />
      </concept>
      <concept id="1664480272136207708" name="com.mbeddr.core.expressions.structure.CharType" flags="ng" index="biTqx" />
      <concept id="8864856114140038681" name="com.mbeddr.core.expressions.structure.DoubleType" flags="ng" index="2fgwQN" />
      <concept id="2212975673976017893" name="com.mbeddr.core.expressions.structure.NumericLiteral" flags="ng" index="2hns93">
        <property id="2212975673976043696" name="value" index="2hmy$m" />
      </concept>
      <concept id="9149785691755093694" name="com.mbeddr.core.expressions.structure.IEEE754TypeSizeSpecification" flags="ng" index="2mYgW_">
        <property id="9149785691755093695" name="exists" index="2mYgW$" />
        <child id="9149785691755093698" name="ieee754Type" index="2mYgXp" />
        <child id="9149785691755093697" name="basicType" index="2mYgXq" />
      </concept>
      <concept id="9149785691754701072" name="com.mbeddr.core.expressions.structure.LongDoubleType" flags="ng" index="2p1N2b" />
      <concept id="4620120465980402700" name="com.mbeddr.core.expressions.structure.GenericDotExpression" flags="ng" index="2qmXGp">
        <child id="7034214596252529803" name="target" index="1ESnxz" />
      </concept>
      <concept id="5763383285156373020" name="com.mbeddr.core.expressions.structure.MultiExpression" flags="ng" index="2BOcij" />
      <concept id="318113533128716675" name="com.mbeddr.core.expressions.structure.ITyped" flags="ng" index="2C2TGh">
        <child id="318113533128716676" name="type" index="2C2TGm" />
      </concept>
      <concept id="8399455261460717640" name="com.mbeddr.core.expressions.structure.AndExpression" flags="ng" index="2EHzL6" />
      <concept id="595416243537320771" name="com.mbeddr.core.expressions.structure.UnsignedShortType" flags="ng" index="LMkMC" />
      <concept id="3335993110369949928" name="com.mbeddr.core.expressions.structure.ShortType" flags="ng" index="MySNB" />
      <concept id="3335993110369795381" name="com.mbeddr.core.expressions.structure.TypeSizeSpecification" flags="ng" index="MXy$U">
        <property id="3335993110370236888" name="exists" index="MzQRn" />
        <child id="7496733358578231499" name="c99Type" index="15Utue" />
        <child id="7496733358578231498" name="basicType" index="15Utuf" />
      </concept>
      <concept id="3335993110369795380" name="com.mbeddr.core.expressions.structure.TypeSizeConfiguration" flags="ng" index="MXy$V">
        <child id="9149785691755067704" name="ieee754Specifications" index="2mYqyz" />
        <child id="3335993110369805710" name="specifications" index="MXv61" />
        <child id="8863019357864392147" name="sizeTType" index="2O5j3Q" />
        <child id="3813668170744198630" name="pointerDiffType" index="3kxMGo" />
        <child id="7808382574383152989" name="intPtrType" index="3sasR9" />
        <child id="7808382574383153001" name="uintPtrType" index="3sasRX" />
        <child id="5598157691785092886" name="vaList" index="3EM3Bk" />
        <child id="6658270785788810330" name="minFloatValue" index="3LaRDq" />
        <child id="6658270785788810339" name="maxDoubleValue" index="3LaRDz" />
        <child id="6658270785788810349" name="minDoubleValue" index="3LaRDH" />
        <child id="6658270785788810029" name="maxFloatValue" index="3LaROH" />
      </concept>
      <concept id="3820836583575227340" name="com.mbeddr.core.expressions.structure.DirectPlusAssignmentExpression" flags="ng" index="TPXPH" />
      <concept id="3830958861296781575" name="com.mbeddr.core.expressions.structure.NotExpression" flags="ng" index="19$8ne" />
      <concept id="7892328519581699353" name="com.mbeddr.core.expressions.structure.VoidType" flags="ng" index="19Rifw" />
      <concept id="3390250080473522603" name="com.mbeddr.core.expressions.structure.SignedCharType" flags="ng" index="1dkrvn" />
      <concept id="22102029902365709" name="com.mbeddr.core.expressions.structure.AssignmentExpr" flags="ng" index="3pqW6w" />
      <concept id="4739982148980385695" name="com.mbeddr.core.expressions.structure.FloatType" flags="ng" index="3AreGT" />
      <concept id="743779816742251347" name="com.mbeddr.core.expressions.structure.ITypeDecorator" flags="ng" index="1Ml8iu">
        <child id="7336544617004013388" name="valueType" index="UxbcT" />
      </concept>
      <concept id="8860443239512147449" name="com.mbeddr.core.expressions.structure.LessExpression" flags="ng" index="3Tl9Jn" />
      <concept id="8860443239512147447" name="com.mbeddr.core.expressions.structure.GreaterEqualsExpression" flags="ng" index="3Tl9Jp" />
      <concept id="8860443239512128058" name="com.mbeddr.core.expressions.structure.BooleanType" flags="ng" index="3TlMgk" />
      <concept id="8860443239512128054" name="com.mbeddr.core.expressions.structure.Type" flags="ng" index="3TlMgo">
        <property id="2941277002445651368" name="const" index="2c7vTL" />
        <property id="2941277002448691247" name="volatile" index="2caQfQ" />
      </concept>
      <concept id="8860443239512128052" name="com.mbeddr.core.expressions.structure.BinaryExpression" flags="ng" index="3TlMgq">
        <child id="8860443239512128064" name="left" index="3TlMhI" />
        <child id="8860443239512128065" name="right" index="3TlMhJ" />
      </concept>
      <concept id="8860443239512128108" name="com.mbeddr.core.expressions.structure.IntType" flags="ng" index="3TlMh2" />
      <concept id="8860443239512128103" name="com.mbeddr.core.expressions.structure.NumberLiteral" flags="ng" index="3TlMh9" />
      <concept id="8860443239512128099" name="com.mbeddr.core.expressions.structure.FalseLiteral" flags="ng" index="3TlMhd" />
      <concept id="8860443239512128094" name="com.mbeddr.core.expressions.structure.TrueLiteral" flags="ng" index="3TlMhK" />
      <concept id="1670233242589902798" name="com.mbeddr.core.expressions.structure.ScientificNumber" flags="ng" index="3VGQI6">
        <property id="1670233242589904217" name="prefix" index="3VGQ4h" />
        <property id="1670233242589904219" name="postfix" index="3VGQ4j" />
      </concept>
      <concept id="86532984527104137" name="com.mbeddr.core.expressions.structure.LongType" flags="ng" index="1X9cn3" />
    </language>
  </registry>
  <node concept="N3F5e" id="15w3mNHPT6P">
    <property role="TrG5h" value="SpaceshipMain" />
    <node concept="1LFe83" id="15w3mNHPT6R" role="N3F5h">
      <property role="TrG5h" value="Spaceship" />
      <ref role="1LFebw" node="15w3mNHPT6U" resolve="parked" />
      <node concept="2cfOFI" id="15w3mNHPT6S" role="1_Iowf">
        <property role="TrG5h" value="power_on" />
      </node>
      <node concept="2cfOFI" id="15w3mNHPT7J" role="1_Iowf">
        <property role="TrG5h" value="power_off" />
      </node>
      <node concept="2cfOFI" id="15w3mNHPT81" role="1_Iowf">
        <property role="TrG5h" value="liftoff" />
      </node>
      <node concept="2cfOFI" id="15w3mNHPT8l" role="1_Iowf">
        <property role="TrG5h" value="accelerate" />
        <node concept="349diW" id="15w3mNHPT8x" role="349dh9">
          <property role="TrG5h" value="a" />
          <node concept="CIVk6" id="6SKWCEilb3A" role="2C2TGm">
            <node concept="26Vqpq" id="6SKWCEilb3_" role="UxbcT">
              <property role="2caQfQ" value="false" />
              <property role="2c7vTL" value="false" />
            </node>
            <node concept="CIsGf" id="6SKWCEilb3B" role="CIVlq">
              <node concept="CIsvn" id="6SKWCEilfpw" role="CIi4h">
                <ref role="CIi3I" node="2svi4LlNfml" resolve="m/s²" />
              </node>
            </node>
          </node>
        </node>
        <node concept="349diW" id="6SKWCEilfvu" role="349dh9">
          <property role="TrG5h" value="t" />
          <node concept="CIVk6" id="6SKWCEilfvQ" role="2C2TGm">
            <node concept="26Vqpq" id="6SKWCEilfvP" role="UxbcT">
              <property role="2caQfQ" value="false" />
              <property role="2c7vTL" value="false" />
            </node>
            <node concept="CIsGf" id="6SKWCEilfvR" role="CIVlq">
              <node concept="CIsvn" id="6SKWCEilfwu" role="CIi4h">
                <ref role="CIi3I" node="2svi4LlNfm1" resolve="s" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2cfOFI" id="15w3mNHPT97" role="1_Iowf">
        <property role="TrG5h" value="land" />
      </node>
      <node concept="2h6h52" id="15w3mNHPT9l" role="1_Iowf" />
      <node concept="1R59hi" id="15w3mNHPT9M" role="1_Iowf">
        <property role="TrG5h" value="velocity" />
        <node concept="CIVk6" id="2svi4LlNi4c" role="2C2TGm">
          <node concept="26Vqpq" id="2svi4LlNi4b" role="UxbcT">
            <property role="2caQfQ" value="false" />
            <property role="2c7vTL" value="false" />
          </node>
          <node concept="CIsGf" id="2svi4LlNi4d" role="CIVlq">
            <node concept="CIsvn" id="2svi4LlNisy" role="CIi4h">
              <ref role="CIi3I" node="2svi4LlNfh5" resolve="m/s" />
            </node>
          </node>
        </node>
        <node concept="CIdvy" id="2svi4LlNj20" role="2cfFcn">
          <node concept="3TlMh9" id="2svi4LlNj1Z" role="CIrOC">
            <property role="2hmy$m" value="0" />
          </node>
          <node concept="CIsGf" id="2svi4LlNj21" role="CIwXZ">
            <node concept="CIsvn" id="2svi4LlNj22" role="CIi4h">
              <ref role="CIi3I" node="2svi4LlNfh5" resolve="m/s" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1R59hi" id="6SKWCEinGaP" role="1_Iowf">
        <property role="TrG5h" value="distance_flown" />
        <node concept="CIVk6" id="6SKWCEinH1p" role="2C2TGm">
          <node concept="26Vqpq" id="6SKWCEinH1o" role="UxbcT">
            <property role="2caQfQ" value="false" />
            <property role="2c7vTL" value="false" />
          </node>
          <node concept="CIsGf" id="6SKWCEinH1q" role="CIVlq">
            <node concept="CIsvn" id="6SKWCEinH2d" role="CIi4h">
              <ref role="CIi3I" node="6SKWCEileWV" resolve="m" />
            </node>
          </node>
        </node>
        <node concept="CIdvy" id="6SKWCEinHKz" role="2cfFcn">
          <node concept="3TlMh9" id="6SKWCEinHKy" role="CIrOC">
            <property role="2hmy$m" value="0" />
          </node>
          <node concept="CIsGf" id="6SKWCEinHK$" role="CIwXZ">
            <node concept="CIsvn" id="6SKWCEinHK_" role="CIi4h">
              <ref role="CIi3I" node="6SKWCEileWV" resolve="m" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1R59hi" id="6SKWCEiocYW" role="1_Iowf">
        <property role="TrG5h" value="travel_time" />
        <node concept="CIVk6" id="6SKWCEiodZS" role="2C2TGm">
          <node concept="26Vqpq" id="6SKWCEiodZR" role="UxbcT">
            <property role="2caQfQ" value="false" />
            <property role="2c7vTL" value="false" />
          </node>
          <node concept="CIsGf" id="6SKWCEiodZT" role="CIVlq">
            <node concept="CIsvn" id="6SKWCEioe0G" role="CIi4h">
              <ref role="CIi3I" node="2svi4LlNfm1" resolve="s" />
            </node>
          </node>
        </node>
        <node concept="CIdvy" id="6SKWCEioewd" role="2cfFcn">
          <node concept="3TlMh9" id="6SKWCEioewc" role="CIrOC">
            <property role="2hmy$m" value="0" />
          </node>
          <node concept="CIsGf" id="6SKWCEioewe" role="CIwXZ">
            <node concept="CIsvn" id="6SKWCEioewf" role="CIi4h">
              <ref role="CIi3I" node="2svi4LlNfm1" resolve="s" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2h6h52" id="6SKWCEiobWO" role="1_Iowf" />
      <node concept="1LFebX" id="15w3mNHPT6U" role="1_Iowf">
        <property role="TrG5h" value="parked" />
        <node concept="1LFeb9" id="15w3mNHPTbd" role="1KoBSX">
          <ref role="1zztin" node="15w3mNHPT75" resolve="parked_engine_on" />
          <node concept="349iI2" id="15w3mNHPTbj" role="2qxFSM">
            <ref role="1bNv6r" node="15w3mNHPT6S" resolve="power_on" />
          </node>
          <node concept="3XIRFW" id="15w3mNHR9SS" role="1zz7TA">
            <node concept="1_9egQ" id="15w3mNHR9T3" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR9T4" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycJV" resolve="printf" />
                <node concept="PhEJO" id="15w3mNHR9T5" role="3O_q_j">
                  <property role="PhEJT" value="Spaceship has been powered on\n" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="15w3mNHR9T6" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR9T7" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycFl" resolve="fflush" />
                <node concept="4ZOvp" id="15w3mNHR9T8" role="3O_q_j">
                  <ref role="2DPCA0" to="3y0n:6Iiej_Uhsyk" resolve="stdout" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1LFebX" id="15w3mNHPT75" role="1_Iowf">
        <property role="TrG5h" value="parked_engine_on" />
        <node concept="1LFeb9" id="15w3mNHPTbo" role="1KoBSX">
          <ref role="1zztin" node="15w3mNHPT7h" resolve="flying" />
          <node concept="349iI2" id="15w3mNHPTbu" role="2qxFSM">
            <ref role="1bNv6r" node="15w3mNHPT81" resolve="liftoff" />
          </node>
          <node concept="3XIRFW" id="15w3mNHR9zS" role="1zz7TA">
            <node concept="1_9egQ" id="15w3mNHR9zX" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR9zY" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycJV" resolve="printf" />
                <node concept="PhEJO" id="15w3mNHR9zZ" role="3O_q_j">
                  <property role="PhEJT" value="Spaceship has lifted off\n" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="15w3mNHR9$0" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR9$1" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycFl" resolve="fflush" />
                <node concept="4ZOvp" id="15w3mNHR9$2" role="3O_q_j">
                  <ref role="2DPCA0" to="3y0n:6Iiej_Uhsyk" resolve="stdout" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="ODFVE" id="15w3mNHR9zD" role="1KoBSX" />
        <node concept="1LFeb9" id="15w3mNHPTb_" role="1KoBSX">
          <ref role="1zztin" node="15w3mNHPT6U" resolve="parked" />
          <node concept="349iI2" id="15w3mNHPTbH" role="2qxFSM">
            <ref role="1bNv6r" node="15w3mNHPT7J" resolve="power_off" />
          </node>
          <node concept="3XIRFW" id="15w3mNHR9tN" role="1zz7TA">
            <node concept="1_9egQ" id="15w3mNHR9u3" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR9u4" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycJV" resolve="printf" />
                <node concept="PhEJO" id="15w3mNHR9u5" role="3O_q_j">
                  <property role="PhEJT" value="Spaceship has parked\n" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="15w3mNHR9u6" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR9u7" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycFl" resolve="fflush" />
                <node concept="4ZOvp" id="15w3mNHR9u8" role="3O_q_j">
                  <ref role="2DPCA0" to="3y0n:6Iiej_Uhsyk" resolve="stdout" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="ODFVE" id="15w3mNHR9tF" role="1KoBSX" />
      </node>
      <node concept="1LFebX" id="15w3mNHPT7h" role="1_Iowf">
        <property role="TrG5h" value="flying" />
        <node concept="1LFeb9" id="15w3mNHPTbM" role="1KoBSX">
          <ref role="1zztin" node="15w3mNHPT7h" resolve="flying" />
          <node concept="349iI2" id="15w3mNHPTbS" role="2qxFSM">
            <ref role="1bNv6r" node="15w3mNHPT8l" resolve="accelerate" />
          </node>
          <node concept="3XIRFW" id="15w3mNHPTbX" role="1zz7TA">
            <node concept="3XIRlf" id="6SKWCEilfCt" role="3XIRFZ">
              <property role="TrG5h" value="dv" />
              <node concept="CIVk6" id="6SKWCEilfCG" role="2C2TGm">
                <node concept="26Vqpq" id="6SKWCEilfCF" role="UxbcT">
                  <property role="2caQfQ" value="false" />
                  <property role="2c7vTL" value="false" />
                </node>
                <node concept="CIsGf" id="6SKWCEilfCH" role="CIVlq">
                  <node concept="CIsvn" id="6SKWCEilfDw" role="CIi4h">
                    <ref role="CIi3I" node="2svi4LlNfh5" resolve="m/s" />
                  </node>
                </node>
              </node>
              <node concept="2BOcij" id="6SKWCEilfVa" role="3XIe9u">
                <node concept="3498Or" id="6SKWCEilg2e" role="3TlMhJ">
                  <ref role="3498Oq" node="6SKWCEilfvu" resolve="t" />
                </node>
                <node concept="3498Or" id="6SKWCEilfIl" role="3TlMhI">
                  <ref role="3498Oq" node="15w3mNHPT8x" resolve="a" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="15w3mNHPTcf" role="3XIRFZ">
              <node concept="TPXPH" id="15w3mNHPThm" role="1_9egR">
                <node concept="349IfM" id="15w3mNHPTcd" role="3TlMhI">
                  <ref role="349IfP" node="15w3mNHPT9M" resolve="velocity" />
                </node>
                <node concept="3ZVu4v" id="6SKWCEilglo" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEilfCt" resolve="dv" />
                </node>
              </node>
            </node>
            <node concept="3XISUE" id="6SKWCEinHML" role="3XIRFZ" />
            <node concept="3XIRlf" id="6SKWCEinJqx" role="3XIRFZ">
              <property role="TrG5h" value="ds" />
              <node concept="CIVk6" id="6SKWCEinJrd" role="2C2TGm">
                <node concept="26Vqpq" id="6SKWCEinJrc" role="UxbcT">
                  <property role="2caQfQ" value="false" />
                  <property role="2c7vTL" value="false" />
                </node>
                <node concept="CIsGf" id="6SKWCEinJre" role="CIVlq">
                  <node concept="CIsvn" id="6SKWCEinKfK" role="CIi4h">
                    <ref role="CIi3I" node="6SKWCEileWV" resolve="m" />
                  </node>
                </node>
              </node>
              <node concept="2BOcij" id="6SKWCEinKE6" role="3XIe9u">
                <node concept="3498Or" id="6SKWCEinLuJ" role="3TlMhJ">
                  <ref role="3498Oq" node="6SKWCEilfvu" resolve="t" />
                </node>
                <node concept="349IfM" id="6SKWCEinKhP" role="3TlMhI">
                  <ref role="349IfP" node="15w3mNHPT9M" resolve="velocity" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="6SKWCEinNbW" role="3XIRFZ">
              <node concept="TPXPH" id="6SKWCEinOwU" role="1_9egR">
                <node concept="349IfM" id="6SKWCEinNbU" role="3TlMhI">
                  <ref role="349IfP" node="6SKWCEinGaP" resolve="distance_flown" />
                </node>
                <node concept="3ZVu4v" id="6SKWCEinOt$" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEinJqx" resolve="ds" />
                </node>
              </node>
            </node>
            <node concept="3XISUE" id="6SKWCEioeyr" role="3XIRFZ" />
            <node concept="1_9egQ" id="6SKWCEiofzq" role="3XIRFZ">
              <node concept="TPXPH" id="6SKWCEiogU9" role="1_9egR">
                <node concept="3498Or" id="6SKWCEiogWH" role="3TlMhJ">
                  <ref role="3498Oq" node="6SKWCEilfvu" resolve="t" />
                </node>
                <node concept="349IfM" id="6SKWCEiofzo" role="3TlMhI">
                  <ref role="349IfP" node="6SKWCEiocYW" resolve="travel_time" />
                </node>
              </node>
            </node>
            <node concept="3XISUE" id="6SKWCEioh3$" role="3XIRFZ" />
            <node concept="1_9egQ" id="6SKWCEipBFu" role="3XIRFZ">
              <node concept="3O_q_g" id="6SKWCEipBFs" role="1_9egR">
                <ref role="3O_q_h" node="6SKWCEio$rI" resolve="printMetric" />
                <node concept="PhEJO" id="6SKWCEipCPi" role="3O_q_j">
                  <property role="PhEJT" value="Current Spaceship velocity: " />
                </node>
                <node concept="2yh1Mg" id="6SKWCEipCQY" role="3O_q_j">
                  <node concept="349IfM" id="6SKWCEipJHC" role="2yh1Mn">
                    <ref role="349IfP" node="15w3mNHPT9M" resolve="velocity" />
                  </node>
                </node>
                <node concept="PhEJO" id="6SKWCEipJN9" role="3O_q_j">
                  <property role="PhEJT" value="m/s" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1LFeb9" id="15w3mNHPTkv" role="1KoBSX">
          <ref role="1zztin" node="15w3mNHPT7v" resolve="crashed" />
          <node concept="349iI2" id="15w3mNHPTkG" role="2qxFSM">
            <ref role="1bNv6r" node="15w3mNHPT97" resolve="land" />
          </node>
          <node concept="3XIRFW" id="15w3mNHPUtF" role="1zz7TA">
            <node concept="1_9egQ" id="15w3mNHPU_P" role="3XIRFZ">
              <node concept="3pqW6w" id="15w3mNHPUEE" role="1_9egR">
                <node concept="CIdvy" id="2svi4LlNkcG" role="3TlMhJ">
                  <node concept="3TlMh9" id="2svi4LlNkcF" role="CIrOC">
                    <property role="2hmy$m" value="0" />
                  </node>
                  <node concept="CIsGf" id="2svi4LlNkcH" role="CIwXZ">
                    <node concept="CIsvn" id="2svi4LlNkcI" role="CIi4h">
                      <ref role="CIi3I" node="2svi4LlNfh5" resolve="m/s" />
                    </node>
                  </node>
                </node>
                <node concept="349IfM" id="15w3mNHPU_N" role="3TlMhI">
                  <ref role="349IfP" node="15w3mNHPT9M" resolve="velocity" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="15w3mNHR7ZG" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR7ZH" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycJV" resolve="printf" />
                <node concept="PhEJO" id="15w3mNHR7ZI" role="3O_q_j">
                  <property role="PhEJT" value="Spaceship has crashed\n" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="15w3mNHR7ZJ" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR7ZK" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycFl" resolve="fflush" />
                <node concept="4ZOvp" id="15w3mNHR7ZL" role="3O_q_j">
                  <ref role="2DPCA0" to="3y0n:6Iiej_Uhsyk" resolve="stdout" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1z9TsT" id="6SKWCEipWZ0" role="lGtFl">
            <node concept="OjmMv" id="6SKWCEipWZ1" role="1w35rA">
              <node concept="19SGf9" id="6SKWCEipWZ2" role="OjmMu">
                <node concept="19SUe$" id="6SKWCEipWZ3" role="19SJt6">
                  <property role="19SUeA" value="If Spaceship flies too fast or backwards, it will crash." />
                </node>
              </node>
            </node>
          </node>
          <node concept="19$8ne" id="6SKWCEiqKgm" role="1zz7me">
            <node concept="3O_q_g" id="6SKWCEiqGUX" role="1_9fRO">
              <ref role="3O_q_h" node="6SKWCEiqcqH" resolve="canLand" />
              <node concept="349IfM" id="6SKWCEiqJ8s" role="3O_q_j">
                <ref role="349IfP" node="15w3mNHPT9M" resolve="velocity" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1LFeb9" id="15w3mNHPU8j" role="1KoBSX">
          <ref role="1zztin" node="15w3mNHPT75" resolve="parked_engine_on" />
          <node concept="349iI2" id="15w3mNHPU8_" role="2qxFSM">
            <ref role="1bNv6r" node="15w3mNHPT97" resolve="land" />
          </node>
          <node concept="3O_q_g" id="6SKWCEiqhhv" role="1zz7me">
            <ref role="3O_q_h" node="6SKWCEiqcqH" resolve="canLand" />
            <node concept="349IfM" id="6SKWCEiqil5" role="3O_q_j">
              <ref role="349IfP" node="15w3mNHPT9M" resolve="velocity" />
            </node>
          </node>
          <node concept="3XIRFW" id="15w3mNHPUVj" role="1zz7TA">
            <node concept="1_9egQ" id="15w3mNHPV3W" role="3XIRFZ">
              <node concept="3pqW6w" id="15w3mNHPV8L" role="1_9egR">
                <node concept="CIdvy" id="2svi4LlNkL6" role="3TlMhJ">
                  <node concept="3TlMh9" id="2svi4LlNkL5" role="CIrOC">
                    <property role="2hmy$m" value="0" />
                  </node>
                  <node concept="CIsGf" id="2svi4LlNkL7" role="CIwXZ">
                    <node concept="CIsvn" id="2svi4LlNkL8" role="CIi4h">
                      <ref role="CIi3I" node="2svi4LlNfh5" resolve="m/s" />
                    </node>
                  </node>
                </node>
                <node concept="349IfM" id="15w3mNHPV3U" role="3TlMhI">
                  <ref role="349IfP" node="15w3mNHPT9M" resolve="velocity" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="15w3mNHR7G3" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR7G1" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycJV" resolve="printf" />
                <node concept="PhEJO" id="15w3mNHR7Gk" role="3O_q_j">
                  <property role="PhEJT" value="Spaceship has landed\n" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="15w3mNHR7YJ" role="3XIRFZ">
              <node concept="3O_q_g" id="15w3mNHR7YH" role="1_9egR">
                <ref role="3O_q_h" to="3y0n:137zkozycFl" resolve="fflush" />
                <node concept="4ZOvp" id="15w3mNHR7Zf" role="3O_q_j">
                  <ref role="2DPCA0" to="3y0n:6Iiej_Uhsyk" resolve="stdout" />
                </node>
              </node>
            </node>
            <node concept="3XISUE" id="6SKWCEip$et" role="3XIRFZ" />
            <node concept="1_9egQ" id="6SKWCEipdvL" role="3XIRFZ">
              <node concept="3O_q_g" id="6SKWCEipdvJ" role="1_9egR">
                <ref role="3O_q_h" node="6SKWCEio$rI" resolve="printMetric" />
                <node concept="PhEJO" id="6SKWCEipeDB" role="3O_q_j">
                  <property role="PhEJT" value="Total distance flown: " />
                </node>
                <node concept="2yh1Mg" id="6SKWCEiph3s" role="3O_q_j">
                  <node concept="349IfM" id="6SKWCEipeEG" role="2yh1Mn">
                    <ref role="349IfP" node="6SKWCEinGaP" resolve="distance_flown" />
                  </node>
                </node>
                <node concept="PhEJO" id="6SKWCEipeFR" role="3O_q_j">
                  <property role="PhEJT" value="m" />
                </node>
              </node>
            </node>
            <node concept="1_9egQ" id="6SKWCEipqUf" role="3XIRFZ">
              <node concept="3O_q_g" id="6SKWCEipqUd" role="1_9egR">
                <ref role="3O_q_h" node="6SKWCEio$rI" resolve="printMetric" />
                <node concept="PhEJO" id="6SKWCEips52" role="3O_q_j">
                  <property role="PhEJT" value="Total travel time: " />
                </node>
                <node concept="2yh1Mg" id="6SKWCEips5P" role="3O_q_j">
                  <node concept="349IfM" id="6SKWCEips66" role="2yh1Mn">
                    <ref role="349IfP" node="6SKWCEiocYW" resolve="travel_time" />
                  </node>
                </node>
                <node concept="PhEJO" id="6SKWCEips7V" role="3O_q_j">
                  <property role="PhEJT" value="s" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1LFebX" id="15w3mNHPT7v" role="1_Iowf">
        <property role="TrG5h" value="crashed" />
      </node>
    </node>
    <node concept="2NXPZ9" id="6SKWCEiowNj" role="N3F5h">
      <property role="TrG5h" value="empty_1578858640817_1" />
    </node>
    <node concept="2B_Gvg" id="6SKWCEiqxgU" role="N3F5h">
      <node concept="OjmMv" id="6SKWCEiqxgW" role="2B_H8o">
        <node concept="19SGf9" id="6SKWCEiqxgX" role="OjmMu">
          <node concept="19SUe$" id="6SKWCEiqxgY" role="19SJt6">
            <property role="19SUeA" value="Checks, if a Spaceship can land. In order to land, a Spaceship must have slow forward velocity." />
          </node>
        </node>
      </node>
    </node>
    <node concept="N3Fnx" id="6SKWCEiqcqH" role="N3F5h">
      <property role="TrG5h" value="canLand" />
      <property role="2OOxQR" value="false" />
      <node concept="3XIRFW" id="6SKWCEiqcqJ" role="3XIRFX">
        <node concept="c0U19" id="6SKWCEiqdFA" role="3XIRFZ">
          <node concept="3XIRFW" id="6SKWCEiqdFB" role="c0U17">
            <node concept="2BFjQ_" id="6SKWCEiqeJq" role="3XIRFZ">
              <node concept="3TlMhK" id="6SKWCEiqeTw" role="2BFjQA" />
            </node>
          </node>
          <node concept="2EHzL6" id="6SKWCEiqeYQ" role="c0U16">
            <node concept="3Tl9Jp" id="6SKWCEiqeYR" role="3TlMhI">
              <node concept="3ZUYvv" id="6SKWCEiqdG7" role="3TlMhI">
                <ref role="3ZUYvu" node="6SKWCEiqdDN" resolve="velocity" />
              </node>
              <node concept="CIdvy" id="6SKWCEiqeYS" role="3TlMhJ">
                <node concept="3TlMh9" id="6SKWCEiqeYT" role="CIrOC">
                  <property role="2hmy$m" value="0" />
                </node>
                <node concept="CIsGf" id="6SKWCEiqeEr" role="CIwXZ">
                  <node concept="CIsvn" id="6SKWCEiqeEs" role="CIi4h">
                    <ref role="CIi3I" node="2svi4LlNfh5" resolve="m/s" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3Tl9Jn" id="6SKWCEiqfse" role="3TlMhJ">
              <node concept="CIdvy" id="6SKWCEiqg5E" role="3TlMhJ">
                <node concept="3TlMh9" id="6SKWCEiqg5D" role="CIrOC">
                  <property role="2hmy$m" value="100" />
                </node>
                <node concept="CIsGf" id="6SKWCEiqg5F" role="CIwXZ">
                  <node concept="CIsvn" id="6SKWCEiqg5G" role="CIi4h">
                    <ref role="CIi3I" node="2svi4LlNfh5" resolve="m/s" />
                  </node>
                </node>
              </node>
              <node concept="3ZUYvv" id="6SKWCEiqf4r" role="3TlMhI">
                <ref role="3ZUYvu" node="6SKWCEiqdDN" resolve="velocity" />
              </node>
            </node>
          </node>
          <node concept="1ly_i6" id="6SKWCEiqeMK" role="ggAap">
            <node concept="3XIRFW" id="6SKWCEiqeML" role="1ly_ph">
              <node concept="2BFjQ_" id="6SKWCEiqeRZ" role="3XIRFZ">
                <node concept="3TlMhd" id="6SKWCEiqeSW" role="2BFjQA" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3TlMgk" id="6SKWCEiqbbX" role="2C2TGm">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="19RgSI" id="6SKWCEiqdDN" role="1UOdpc">
        <property role="TrG5h" value="velocity" />
        <node concept="CIVk6" id="6SKWCEiqdEo" role="2C2TGm">
          <node concept="26Vqpq" id="6SKWCEiqdEn" role="UxbcT">
            <property role="2caQfQ" value="false" />
            <property role="2c7vTL" value="false" />
          </node>
          <node concept="CIsGf" id="6SKWCEiqdEp" role="CIVlq">
            <node concept="CIsvn" id="6SKWCEiqdF0" role="CIi4h">
              <ref role="CIi3I" node="2svi4LlNfh5" resolve="m/s" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2NXPZ9" id="6SKWCEiq8IY" role="N3F5h">
      <property role="TrG5h" value="empty_1578859140684_5" />
    </node>
    <node concept="N3Fnx" id="6SKWCEio$rI" role="N3F5h">
      <property role="TrG5h" value="printMetric" />
      <property role="2OOxQR" value="false" />
      <node concept="3XIRFW" id="6SKWCEio$rK" role="3XIRFX">
        <node concept="3XISUE" id="6SKWCEio$rL" role="3XIRFZ" />
        <node concept="1_9egQ" id="6SKWCEio_GF" role="3XIRFZ">
          <node concept="3O_q_g" id="6SKWCEio_GG" role="1_9egR">
            <ref role="3O_q_h" to="3y0n:137zkozycJV" resolve="printf" />
            <node concept="3ZUYvv" id="6SKWCEioFrE" role="3O_q_j">
              <ref role="3ZUYvu" node="6SKWCEio_Cy" resolve="description" />
            </node>
          </node>
        </node>
        <node concept="3XIRlf" id="6SKWCEio_GI" role="3XIRFZ">
          <property role="TrG5h" value="x" />
          <node concept="3J0A42" id="6SKWCEio_GJ" role="2C2TGm">
            <property role="2caQfQ" value="false" />
            <property role="2c7vTL" value="false" />
            <node concept="biTqx" id="6SKWCEio_GK" role="2umbIo">
              <property role="2caQfQ" value="false" />
              <property role="2c7vTL" value="false" />
            </node>
            <node concept="3TlMh9" id="6SKWCEio_GL" role="1YbSNA">
              <property role="2hmy$m" value="20" />
            </node>
          </node>
        </node>
        <node concept="1QiMYF" id="6SKWCEio_GM" role="3XIRFZ">
          <node concept="OjmMv" id="6SKWCEio_GN" role="3SJzmv">
            <node concept="19SGf9" id="6SKWCEio_GO" role="OjmMu">
              <node concept="19SUe$" id="6SKWCEio_GP" role="19SJt6">
                <property role="19SUeA" value="Bei Conversion TypeSizeConfiguration beachten!" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="6SKWCEio_GQ" role="3XIRFZ">
          <node concept="3O_q_g" id="6SKWCEio_GR" role="1_9egR">
            <ref role="3O_q_h" to="3y0n:137zkozycKc" resolve="sprintf" />
            <node concept="3ZVu4v" id="6SKWCEio_GS" role="3O_q_j">
              <ref role="3ZVs_2" node="6SKWCEio_GI" resolve="x" />
            </node>
            <node concept="PhEJO" id="6SKWCEio_GT" role="3O_q_j">
              <property role="PhEJT" value="%hi" />
            </node>
            <node concept="3ZUYvv" id="6SKWCEioAVE" role="3O_q_j">
              <ref role="3ZUYvu" node="6SKWCEio_EH" resolve="metric" />
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="6SKWCEio_GV" role="3XIRFZ">
          <node concept="3O_q_g" id="6SKWCEio_GW" role="1_9egR">
            <ref role="3O_q_h" to="3y0n:137zkozycJV" resolve="printf" />
            <node concept="3ZVu4v" id="6SKWCEio_GX" role="3O_q_j">
              <ref role="3ZVs_2" node="6SKWCEio_GI" resolve="x" />
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="6SKWCEio_GY" role="3XIRFZ">
          <node concept="3O_q_g" id="6SKWCEio_GZ" role="1_9egR">
            <ref role="3O_q_h" to="3y0n:137zkozycJV" resolve="printf" />
            <node concept="3ZUYvv" id="6SKWCEioVOp" role="3O_q_j">
              <ref role="3ZUYvu" node="6SKWCEio_Fo" resolve="unit" />
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="6SKWCEip4dU" role="3XIRFZ">
          <node concept="3O_q_g" id="6SKWCEip4dS" role="1_9egR">
            <ref role="3O_q_h" to="3y0n:137zkozycJV" resolve="printf" />
            <node concept="PhEJO" id="6SKWCEip5mQ" role="3O_q_j">
              <property role="PhEJT" value="\n" />
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="6SKWCEio_LF" role="3XIRFZ">
          <node concept="3O_q_g" id="6SKWCEio_LD" role="1_9egR">
            <ref role="3O_q_h" to="3y0n:137zkozycFl" resolve="fflush" />
            <node concept="4ZOvp" id="6SKWCEioAV7" role="3O_q_j">
              <ref role="2DPCA0" to="3y0n:6Iiej_Uhsyk" resolve="stdout" />
            </node>
          </node>
        </node>
      </node>
      <node concept="19Rifw" id="6SKWCEiozgm" role="2C2TGm">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="19RgSI" id="6SKWCEio_Cy" role="1UOdpc">
        <property role="TrG5h" value="description" />
        <node concept="Pu267" id="6SKWCEio_Cx" role="2C2TGm">
          <property role="2caQfQ" value="false" />
          <property role="2c7vTL" value="false" />
        </node>
      </node>
      <node concept="19RgSI" id="6SKWCEio_EH" role="1UOdpc">
        <property role="TrG5h" value="metric" />
        <node concept="26Vqpq" id="6SKWCEipigf" role="2C2TGm">
          <property role="2caQfQ" value="false" />
          <property role="2c7vTL" value="false" />
        </node>
      </node>
      <node concept="19RgSI" id="6SKWCEio_Fo" role="1UOdpc">
        <property role="TrG5h" value="unit" />
        <node concept="Pu267" id="6SKWCEio_Fm" role="2C2TGm">
          <property role="2caQfQ" value="false" />
          <property role="2c7vTL" value="false" />
        </node>
      </node>
    </node>
    <node concept="2NXPZ9" id="15w3mNHPVqm" role="N3F5h">
      <property role="TrG5h" value="empty_1578558977291_62" />
    </node>
    <node concept="N3Fnx" id="15w3mNHPV$P" role="N3F5h">
      <property role="TrG5h" value="main" />
      <property role="2OOxQR" value="true" />
      <node concept="3XIRFW" id="15w3mNHPV$R" role="3XIRFX">
        <node concept="3XIRlf" id="15w3mNHPVL3" role="3XIRFZ">
          <property role="TrG5h" value="xwing" />
          <node concept="3lBjsv" id="15w3mNHPVL1" role="2C2TGm">
            <property role="2caQfQ" value="false" />
            <property role="2c7vTL" value="false" />
            <ref role="3lBjss" node="15w3mNHPT6R" resolve="Spaceship" />
          </node>
        </node>
        <node concept="1_9egQ" id="15w3mNHPVSm" role="3XIRFZ">
          <node concept="2qmXGp" id="15w3mNHPVVt" role="1_9egR">
            <node concept="Vf_e3" id="15w3mNHPVXz" role="1ESnxz" />
            <node concept="3ZVu4v" id="15w3mNHPVSk" role="1_9fRO">
              <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
            </node>
          </node>
        </node>
        <node concept="3XISUE" id="15w3mNHPVXO" role="3XIRFZ" />
        <node concept="3XIRlf" id="6SKWCEildNc" role="3XIRFZ">
          <property role="TrG5h" value="dt" />
          <node concept="CIVk6" id="6SKWCEildOf" role="2C2TGm">
            <node concept="26Vqpq" id="6SKWCEildOe" role="UxbcT">
              <property role="2caQfQ" value="false" />
              <property role="2c7vTL" value="false" />
            </node>
            <node concept="CIsGf" id="6SKWCEildOg" role="CIVlq">
              <node concept="CIsvn" id="6SKWCEildPK" role="CIi4h">
                <ref role="CIi3I" node="2svi4LlNfm1" resolve="s" />
              </node>
            </node>
          </node>
          <node concept="CIdvy" id="6SKWCEileQM" role="3XIe9u">
            <node concept="3TlMh9" id="6SKWCEileQL" role="CIrOC">
              <property role="2hmy$m" value="10" />
            </node>
            <node concept="CIsGf" id="6SKWCEileQN" role="CIwXZ">
              <node concept="CIsvn" id="6SKWCEileQO" role="CIi4h">
                <ref role="CIi3I" node="2svi4LlNfm1" resolve="s" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3XIRlf" id="6SKWCEilR6u" role="3XIRFZ">
          <property role="TrG5h" value="a" />
          <node concept="CIVk6" id="6SKWCEilR7t" role="2C2TGm">
            <node concept="26Vqpq" id="6SKWCEilR7s" role="UxbcT">
              <property role="2caQfQ" value="false" />
              <property role="2c7vTL" value="false" />
            </node>
            <node concept="CIsGf" id="6SKWCEilR7u" role="CIVlq">
              <node concept="CIsvn" id="6SKWCEilR8h" role="CIi4h">
                <ref role="CIi3I" node="2svi4LlNfml" resolve="m/s²" />
              </node>
            </node>
          </node>
          <node concept="CIdvy" id="6SKWCEilRCA" role="3XIe9u">
            <node concept="3TlMh9" id="6SKWCEilRC_" role="CIrOC">
              <property role="2hmy$m" value="10" />
            </node>
            <node concept="CIsGf" id="6SKWCEilRCB" role="CIwXZ">
              <node concept="CIsvn" id="6SKWCEilRCC" role="CIi4h">
                <ref role="CIi3I" node="2svi4LlNfml" resolve="m/s²" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3XISUE" id="6SKWCEilR5k" role="3XIRFZ" />
        <node concept="1_9egQ" id="15w3mNHPW6A" role="3XIRFZ">
          <node concept="2qmXGp" id="15w3mNHPW8F" role="1_9egR">
            <node concept="$QhJh" id="15w3mNHPWaU" role="1ESnxz">
              <ref role="$QhfV" node="15w3mNHPT6S" resolve="power_on" />
            </node>
            <node concept="3ZVu4v" id="15w3mNHPW6$" role="1_9fRO">
              <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="6SKWCEimFcW" role="3XIRFZ">
          <node concept="2qmXGp" id="6SKWCEimFe3" role="1_9egR">
            <node concept="$QhJh" id="6SKWCEimFrT" role="1ESnxz">
              <ref role="$QhfV" node="15w3mNHPT81" resolve="liftoff" />
            </node>
            <node concept="3ZVu4v" id="6SKWCEimFcU" role="1_9fRO">
              <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
            </node>
          </node>
        </node>
        <node concept="3XISUE" id="6SKWCEimFbJ" role="3XIRFZ" />
        <node concept="1_9egQ" id="15w3mNHPWkD" role="3XIRFZ">
          <node concept="2qmXGp" id="15w3mNHPWmQ" role="1_9egR">
            <node concept="$QhJh" id="15w3mNHPWpn" role="1ESnxz">
              <ref role="$QhfV" node="15w3mNHPT8l" resolve="accelerate" />
              <node concept="2BOcij" id="6SKWCEilTp_" role="$QhfN">
                <node concept="3ZVu4v" id="6SKWCEilTGn" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEilR6u" resolve="a" />
                </node>
                <node concept="3TlMh9" id="6SKWCEilT1o" role="3TlMhI">
                  <property role="2hmy$m" value="7" />
                </node>
              </node>
              <node concept="2BOcij" id="6SKWCEilUx1" role="$QhfN">
                <node concept="3ZVu4v" id="6SKWCEilUKG" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEildNc" resolve="dt" />
                </node>
                <node concept="3TlMh9" id="6SKWCEilTMH" role="3TlMhI">
                  <property role="2hmy$m" value="6" />
                </node>
              </node>
            </node>
            <node concept="3ZVu4v" id="15w3mNHPWkB" role="1_9fRO">
              <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="6SKWCEimICf" role="3XIRFZ">
          <node concept="2qmXGp" id="6SKWCEimIDr" role="1_9egR">
            <node concept="$QhJh" id="6SKWCEimIRq" role="1ESnxz">
              <ref role="$QhfV" node="15w3mNHPT8l" resolve="accelerate" />
              <node concept="2BOcij" id="6SKWCEimJnM" role="$QhfN">
                <node concept="3ZVu4v" id="6SKWCEimJoU" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEilR6u" resolve="a" />
                </node>
                <node concept="3TlMh9" id="6SKWCEimIRF" role="3TlMhI">
                  <property role="2hmy$m" value="5" />
                </node>
              </node>
              <node concept="2BOcij" id="6SKWCEimK7J" role="$QhfN">
                <node concept="3ZVu4v" id="6SKWCEimKpC" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEildNc" resolve="dt" />
                </node>
                <node concept="3TlMh9" id="6SKWCEimJx0" role="3TlMhI">
                  <property role="2hmy$m" value="20" />
                </node>
              </node>
            </node>
            <node concept="3ZVu4v" id="6SKWCEimICd" role="1_9fRO">
              <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="15w3mNHPWSn" role="3XIRFZ">
          <node concept="2qmXGp" id="15w3mNHPWUM" role="1_9egR">
            <node concept="$QhJh" id="15w3mNHPWYZ" role="1ESnxz">
              <ref role="$QhfV" node="15w3mNHPT8l" resolve="accelerate" />
              <node concept="2BOcij" id="6SKWCEilVrE" role="$QhfN">
                <node concept="3ZVu4v" id="6SKWCEilVwI" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEilR6u" resolve="a" />
                </node>
                <node concept="3TlMh9" id="6SKWCEilVjq" role="3TlMhI">
                  <property role="2hmy$m" value="-35" />
                </node>
              </node>
              <node concept="2BOcij" id="6SKWCEilW7p" role="$QhfN">
                <node concept="3ZVu4v" id="6SKWCEilWjj" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEildNc" resolve="dt" />
                </node>
                <node concept="3TlMh9" id="6SKWCEilVBn" role="3TlMhI">
                  <property role="2hmy$m" value="4" />
                </node>
              </node>
            </node>
            <node concept="3ZVu4v" id="15w3mNHPWSl" role="1_9fRO">
              <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="6SKWCEin_yC" role="3XIRFZ">
          <node concept="2qmXGp" id="6SKWCEin_yD" role="1_9egR">
            <node concept="$QhJh" id="6SKWCEin_yE" role="1ESnxz">
              <ref role="$QhfV" node="15w3mNHPT8l" resolve="accelerate" />
              <node concept="2BOcij" id="6SKWCEin_yF" role="$QhfN">
                <node concept="3ZVu4v" id="6SKWCEin_yG" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEilR6u" resolve="a" />
                </node>
                <node concept="3TlMh9" id="6SKWCEin_yH" role="3TlMhI">
                  <property role="2hmy$m" value="-1" />
                </node>
              </node>
              <node concept="2BOcij" id="6SKWCEin_yI" role="$QhfN">
                <node concept="3ZVu4v" id="6SKWCEin_yJ" role="3TlMhJ">
                  <ref role="3ZVs_2" node="6SKWCEildNc" resolve="dt" />
                </node>
                <node concept="3TlMh9" id="6SKWCEin_yK" role="3TlMhI">
                  <property role="2hmy$m" value="2" />
                </node>
              </node>
            </node>
            <node concept="3ZVu4v" id="6SKWCEin_yL" role="1_9fRO">
              <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
            </node>
          </node>
        </node>
        <node concept="3XISUE" id="6SKWCEiqV4M" role="3XIRFZ" />
        <node concept="1QiMYF" id="6SKWCEiqV7Y" role="3XIRFZ">
          <node concept="OjmMv" id="6SKWCEiqV80" role="3SJzmv">
            <node concept="19SGf9" id="6SKWCEiqV81" role="OjmMu">
              <node concept="19SUe$" id="6SKWCEiqV82" role="19SJt6">
                <property role="19SUeA" value="comment this out, else the spaceship will crash" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1X3_iC" id="6SKWCEiqV9H" role="lGtFl">
          <property role="3V$3am" value="statements" />
          <property role="3V$3ak" value="a9d69647-0840-491e-bf39-2eb0805d2011/4185783222026475861/4185783222026475862" />
          <node concept="1_9egQ" id="6SKWCEiqOZ5" role="8Wnug">
            <node concept="2qmXGp" id="6SKWCEiqP0_" role="1_9egR">
              <node concept="$QhJh" id="6SKWCEiqPr$" role="1ESnxz">
                <ref role="$QhfV" node="15w3mNHPT8l" resolve="accelerate" />
                <node concept="2BOcij" id="6SKWCEiqPVB" role="$QhfN">
                  <node concept="3ZVu4v" id="6SKWCEiqPWN" role="3TlMhJ">
                    <ref role="3ZVs_2" node="6SKWCEilR6u" resolve="a" />
                  </node>
                  <node concept="3TlMh9" id="6SKWCEiqPrP" role="3TlMhI">
                    <property role="2hmy$m" value="3" />
                  </node>
                </node>
                <node concept="2BOcij" id="6SKWCEiqQRT" role="$QhfN">
                  <node concept="3ZVu4v" id="6SKWCEiqRnG" role="3TlMhJ">
                    <ref role="3ZVs_2" node="6SKWCEildNc" resolve="dt" />
                  </node>
                  <node concept="3TlMh9" id="6SKWCEiqQ4T" role="3TlMhI">
                    <property role="2hmy$m" value="60" />
                  </node>
                </node>
              </node>
              <node concept="3ZVu4v" id="6SKWCEiqOZ3" role="1_9fRO">
                <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3XISUE" id="6SKWCEimKx_" role="3XIRFZ" />
        <node concept="1_9egQ" id="15w3mNHPX2k" role="3XIRFZ">
          <node concept="2qmXGp" id="15w3mNHPX4O" role="1_9egR">
            <node concept="$QhJh" id="15w3mNHPX9e" role="1ESnxz">
              <ref role="$QhfV" node="15w3mNHPT97" resolve="land" />
            </node>
            <node concept="3ZVu4v" id="15w3mNHPX2i" role="1_9fRO">
              <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
            </node>
          </node>
        </node>
        <node concept="1_9egQ" id="15w3mNHQsEI" role="3XIRFZ">
          <node concept="2qmXGp" id="15w3mNHQsHl" role="1_9egR">
            <node concept="$QhJh" id="15w3mNHQsLS" role="1ESnxz">
              <ref role="$QhfV" node="15w3mNHPT7J" resolve="power_off" />
            </node>
            <node concept="3ZVu4v" id="15w3mNHQsEG" role="1_9fRO">
              <ref role="3ZVs_2" node="15w3mNHPVL3" resolve="xwing" />
            </node>
          </node>
        </node>
        <node concept="3XISUE" id="15w3mNHRsSw" role="3XIRFZ" />
        <node concept="2BFjQ_" id="15w3mNHPV$Z" role="3XIRFZ">
          <node concept="3TlMh9" id="15w3mNHPV_0" role="2BFjQA">
            <property role="2hmy$m" value="0" />
          </node>
        </node>
      </node>
      <node concept="26Vqph" id="15w3mNHPV$T" role="2C2TGm">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="19RgSI" id="15w3mNHPV$U" role="1UOdpc">
        <property role="TrG5h" value="argc" />
        <node concept="26Vqph" id="15w3mNHPV$V" role="2C2TGm">
          <property role="2caQfQ" value="false" />
          <property role="2c7vTL" value="false" />
        </node>
      </node>
      <node concept="19RgSI" id="15w3mNHPV$W" role="1UOdpc">
        <property role="TrG5h" value="argv" />
        <node concept="3J0A42" id="15w3mNHPV$X" role="2C2TGm">
          <property role="2caQfQ" value="false" />
          <property role="2c7vTL" value="false" />
          <node concept="Pu267" id="15w3mNHPV$Y" role="2umbIo">
            <property role="2caQfQ" value="false" />
            <property role="2c7vTL" value="false" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3GEVxB" id="15w3mNHQsn3" role="2OODSX">
      <ref role="3GEb4d" to="3y0n:1WTn9U1aQF1" resolve="stdio" />
    </node>
    <node concept="3GEVxB" id="2svi4LlNimm" role="2OODSX">
      <ref role="3GEb4d" node="2svi4LlNfgH" resolve="Units" />
    </node>
  </node>
  <node concept="2v9HqL" id="15w3mNHPXdB">
    <node concept="2eOfOl" id="15w3mNHPXdE" role="2ePNbc">
      <property role="iO3LB" value="false" />
      <property role="TrG5h" value="Spaceship" />
      <ref role="3oK8_y" node="15w3mNHQkwI" resolve="Win32" />
      <node concept="2v9HqM" id="15w3mNHQtfe" role="2eOfOg">
        <ref role="2v9HqP" node="15w3mNHPT6P" resolve="SpaceshipMain" />
      </node>
      <node concept="2v9HqM" id="15w3mNHQPC8" role="2eOfOg">
        <ref role="2v9HqP" to="3y0n:1WTn9U1b1j1" resolve="stdlib" />
      </node>
      <node concept="2v9HqM" id="15w3mNHQPCg" role="2eOfOg">
        <ref role="2v9HqP" to="3y0n:1WTn9U1aQF1" resolve="stdio" />
      </node>
      <node concept="2v9HqM" id="15w3mNHQPCq" role="2eOfOg">
        <ref role="2v9HqP" to="3y0n:137zkozycPF" resolve="stdarg" />
      </node>
    </node>
    <node concept="2Q9Fgs" id="15w3mNHQ6eN" role="2Q9xDr">
      <node concept="2Q9FjX" id="15w3mNHQ6eO" role="2Q9FjI" />
    </node>
    <node concept="3yF7LM" id="15w3mNHPXdI" role="2Q9xDr">
      <property role="3yF7Mc" value="true" />
    </node>
    <node concept="2eh4Hv" id="2svi4LlNlXg" role="2Q9xDr" />
    <node concept="2AWWZL" id="15w3mNHQkwH" role="2AWWZH">
      <property role="uKT8v" value="true" />
      <property role="2AWWZJ" value="gcc" />
      <property role="3r8Kw1" value="gdb" />
      <property role="3r8Kxs" value="make" />
      <property role="2AWWZI" value="-std=c99" />
      <property role="1FkSt$" value="-g" />
      <node concept="3abb7c" id="15w3mNHQkwI" role="3anu1O">
        <property role="TrG5h" value="Win32" />
      </node>
      <node concept="3abb7c" id="15w3mNHQkwJ" role="3anu1O">
        <property role="TrG5h" value="Linux" />
      </node>
      <node concept="3abb7c" id="15w3mNHQkwK" role="3anu1O">
        <property role="TrG5h" value="portable" />
      </node>
      <node concept="3abb7c" id="15w3mNHQkwL" role="3anu1O">
        <property role="TrG5h" value="MacOSX" />
      </node>
    </node>
  </node>
  <node concept="MXy$V" id="15w3mNHR1yX">
    <node concept="26Vqpb" id="15w3mNHR1yY" role="3kxMGo">
      <property role="2caQfQ" value="false" />
      <property role="2c7vTL" value="false" />
    </node>
    <node concept="1X9cn3" id="15w3mNHR1yZ" role="3sasR9">
      <property role="2caQfQ" value="false" />
      <property role="2c7vTL" value="false" />
    </node>
    <node concept="26VBNf" id="15w3mNHR1z0" role="3sasRX">
      <property role="2caQfQ" value="false" />
      <property role="2c7vTL" value="false" />
    </node>
    <node concept="26Vqpb" id="15w3mNHR1z1" role="2O5j3Q">
      <property role="2caQfQ" value="false" />
      <property role="2c7vTL" value="false" />
    </node>
    <node concept="26Vqpb" id="15w3mNHR1z2" role="3EM3Bk">
      <property role="2caQfQ" value="false" />
      <property role="2c7vTL" value="false" />
    </node>
    <node concept="3VGQI6" id="15w3mNHR1z4" role="3LaRDq">
      <property role="3VGQ4h" value="-1" />
      <property role="3VGQ4j" value="37" />
    </node>
    <node concept="3VGQI6" id="15w3mNHR1z5" role="3LaRDH">
      <property role="3VGQ4h" value="-1" />
      <property role="3VGQ4j" value="37" />
    </node>
    <node concept="3VGQI6" id="15w3mNHR1z6" role="3LaROH">
      <property role="3VGQ4h" value="1" />
      <property role="3VGQ4j" value="37" />
    </node>
    <node concept="3VGQI6" id="15w3mNHR1z7" role="3LaRDz">
      <property role="3VGQ4h" value="1" />
      <property role="3VGQ4j" value="37" />
    </node>
    <node concept="MXy$U" id="15w3mNHR1za" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="1dkrvn" id="15w3mNHR1z8" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26Vqqz" id="15w3mNHR1z9" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="MXy$U" id="15w3mNHR1zd" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="MySNB" id="15w3mNHR1zb" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26Vqpq" id="15w3mNHR1zc" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="MXy$U" id="15w3mNHR1zg" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="3TlMh2" id="15w3mNHR1ze" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26Vqph" id="15w3mNHR1zf" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="MXy$U" id="15w3mNHR1zj" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="1X9cn3" id="15w3mNHR1zh" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26Vqpk" id="15w3mNHR1zi" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="MXy$U" id="15w3mNHR1zm" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="26VBN5" id="15w3mNHR1zk" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26Vqpk" id="15w3mNHR1zl" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="MXy$U" id="15w3mNHR1zp" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="26VXeP" id="15w3mNHR1zn" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26Vqp4" id="15w3mNHR1zo" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="MXy$U" id="15w3mNHR1zs" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="LMkMC" id="15w3mNHR1zq" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26VqpV" id="15w3mNHR1zr" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="MXy$U" id="15w3mNHR1zv" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="26VXez" id="15w3mNHR1zt" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26Vqpb" id="15w3mNHR1zu" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="MXy$U" id="15w3mNHR1zy" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="26VBNf" id="15w3mNHR1zw" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26Vqp1" id="15w3mNHR1zx" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="MXy$U" id="15w3mNHR1z_" role="MXv61">
      <property role="MzQRn" value="true" />
      <node concept="26VqpY" id="15w3mNHR1zz" role="15Utuf">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="26Vqp1" id="15w3mNHR1z$" role="15Utue">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="2mYgW_" id="15w3mNHR1zC" role="2mYqyz">
      <property role="2mYgW$" value="true" />
      <node concept="3AreGT" id="15w3mNHR1zA" role="2mYgXq">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="3AreGT" id="15w3mNHR1zB" role="2mYgXp">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="2mYgW_" id="15w3mNHR1zF" role="2mYqyz">
      <property role="2mYgW$" value="true" />
      <node concept="2fgwQN" id="15w3mNHR1zD" role="2mYgXq">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="2fgwQN" id="15w3mNHR1zE" role="2mYgXp">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
    <node concept="2mYgW_" id="15w3mNHR1zI" role="2mYqyz">
      <property role="2mYgW$" value="true" />
      <node concept="2p1N2b" id="15w3mNHR1zG" role="2mYgXq">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
      <node concept="2p1N2b" id="15w3mNHR1zH" role="2mYgXp">
        <property role="2caQfQ" value="false" />
        <property role="2c7vTL" value="false" />
      </node>
    </node>
  </node>
  <node concept="CIrOI" id="2svi4LlNfgH">
    <property role="TrG5h" value="Units" />
    <node concept="CIrOH" id="2svi4LlNfm1" role="CIrPi">
      <property role="2OOxQR" value="true" />
      <property role="TrG5h" value="s" />
      <property role="CIruq" value="time" />
    </node>
    <node concept="CIrOH" id="6SKWCEileWV" role="CIrPi">
      <property role="2OOxQR" value="true" />
      <property role="TrG5h" value="m" />
      <property role="CIruq" value="distance" />
    </node>
    <node concept="CIrOH" id="2svi4LlNfh5" role="CIrPi">
      <property role="2OOxQR" value="true" />
      <property role="TrG5h" value="m/s" />
      <property role="CIruq" value="velocity" />
      <node concept="CIsGf" id="6SKWCEileXo" role="CIsG9">
        <node concept="CIsvn" id="6SKWCEilf3g" role="CIi4h">
          <ref role="CIi3I" node="6SKWCEileWV" resolve="m" />
        </node>
        <node concept="CIsvn" id="6SKWCEilf3m" role="CIi4h">
          <ref role="CIi3I" node="2svi4LlNfm1" resolve="s" />
          <node concept="CIsvk" id="6SKWCEilf3q" role="CIi3G">
            <property role="CIsvl" value="-1" />
          </node>
        </node>
      </node>
    </node>
    <node concept="CIrOH" id="2svi4LlNfml" role="CIrPi">
      <property role="2OOxQR" value="true" />
      <property role="TrG5h" value="m/s²" />
      <property role="CIruq" value="acceleration" />
      <node concept="CIsGf" id="2svi4LlNfmm" role="CIsG9">
        <node concept="CIsvn" id="6SKWCEileWw" role="CIi4h">
          <ref role="CIi3I" node="2svi4LlNfh5" resolve="m/s" />
        </node>
        <node concept="CIsvn" id="6SKWCEilf3v" role="CIi4h">
          <ref role="CIi3I" node="2svi4LlNfm1" resolve="s" />
          <node concept="CIsvk" id="6SKWCEilf3z" role="CIi3G">
            <property role="CIsvl" value="-1" />
          </node>
        </node>
      </node>
    </node>
    <node concept="134lye" id="2svi4LlNflU" role="CIrPi" />
  </node>
</model>

