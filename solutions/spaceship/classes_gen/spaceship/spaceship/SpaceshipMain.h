#ifndef SPACESHIPMAIN_H
#define SPACESHIPMAIN_H


#include <stdint.h>

#include <stddef.h>

#include <stdbool.h>



#ifdef __cplusplus
extern "C" {
#endif

int32_t SpaceshipMain_main(int32_t argc, char *(argv[]));


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
