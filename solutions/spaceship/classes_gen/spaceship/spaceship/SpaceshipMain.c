#include "SpaceshipMain.h"


#include <stdio.h>
#include "Units.h"

/* 
 * Checks, if a Spaceship can land. In order to land, a Spaceship must have slow forward velocity.
 */

enum SpaceshipMain_Spaceship__inevents{
  SpaceshipMain_Spaceship__inevents__Spaceship_power_on__event,
  SpaceshipMain_Spaceship__inevents__Spaceship_power_off__event,
  SpaceshipMain_Spaceship__inevents__Spaceship_liftoff__event,
  SpaceshipMain_Spaceship__inevents__Spaceship_accelerate__event,
  SpaceshipMain_Spaceship__inevents__Spaceship_land__event
};

typedef enum SpaceshipMain_Spaceship__inevents SpaceshipMain_Spaceship__inevents_t;
enum SpaceshipMain_Spaceship__states{
  SpaceshipMain_Spaceship__states__Spaceship_parked__state,
  SpaceshipMain_Spaceship__states__Spaceship_parked_engine_on__state,
  SpaceshipMain_Spaceship__states__Spaceship_flying__state,
  SpaceshipMain_Spaceship__states__Spaceship_crashed__state
};

typedef enum SpaceshipMain_Spaceship__states SpaceshipMain_Spaceship__states_t;
typedef struct SpaceshipMain_Spaceship__data SpaceshipMain_Spaceship__data_t;
struct SpaceshipMain_Spaceship__data {
  SpaceshipMain_Spaceship__states_t __currentState;
  int16_t velocity;
  int16_t distance_flown;
  int16_t travel_time;
};

static void SpaceshipMain_Spaceship__init(SpaceshipMain_Spaceship__data_t *instance);

static void SpaceshipMain_Spaceship__execute(SpaceshipMain_Spaceship__data_t *instance, SpaceshipMain_Spaceship__inevents_t event, void **arguments);

static bool SpaceshipMain_canLand(int16_t velocity);

static void SpaceshipMain_printMetric(char *description, int16_t metric, char *unit);

static void SpaceshipMain_Spaceship__init(SpaceshipMain_Spaceship__data_t *instance) 
{
  instance->__currentState = SpaceshipMain_Spaceship__states__Spaceship_parked__state;
  instance->velocity = 0;
  instance->distance_flown = 0;
  instance->travel_time = 0;
}


static void SpaceshipMain_Spaceship__execute(SpaceshipMain_Spaceship__data_t *instance, SpaceshipMain_Spaceship__inevents_t event, void **arguments) 
{
  bool __isEpsilonEvent;
  do{
    __isEpsilonEvent = true;
    switch (instance->__currentState)
    {
      case SpaceshipMain_Spaceship__states__Spaceship_parked__state: {
        switch (event)
        {
          case SpaceshipMain_Spaceship__inevents__Spaceship_power_on__event: {
            if (true) 
            {
              {
                /* 
                 * transition actions
                 */

                printf("Spaceship has been powered on\n");
                fflush(stdout);
              }

              /* 
               * switch state
               */

              instance->__currentState = SpaceshipMain_Spaceship__states__Spaceship_parked_engine_on__state;
              __isEpsilonEvent &= true;
              break;
            }

            break;
          }
        }

        break;
      }
      case SpaceshipMain_Spaceship__states__Spaceship_parked_engine_on__state: {
        switch (event)
        {
          case SpaceshipMain_Spaceship__inevents__Spaceship_liftoff__event: {
            if (true) 
            {
              {
                /* 
                 * transition actions
                 */

                printf("Spaceship has lifted off\n");
                fflush(stdout);
              }

              /* 
               * switch state
               */

              instance->__currentState = SpaceshipMain_Spaceship__states__Spaceship_flying__state;
              __isEpsilonEvent &= true;
              break;
            }

            break;
          }
          case SpaceshipMain_Spaceship__inevents__Spaceship_power_off__event: {
            if (true) 
            {
              {
                /* 
                 * transition actions
                 */

                printf("Spaceship has parked\n");
                fflush(stdout);
              }

              /* 
               * switch state
               */

              instance->__currentState = SpaceshipMain_Spaceship__states__Spaceship_parked__state;
              __isEpsilonEvent &= true;
              break;
            }

            break;
          }
        }

        break;
      }
      case SpaceshipMain_Spaceship__states__Spaceship_flying__state: {
        switch (event)
        {
          case SpaceshipMain_Spaceship__inevents__Spaceship_accelerate__event: {
            if (true) 
            {
              {
                /* 
                 * transition actions
                 */

                int16_t dv = (*((int16_t *)((arguments[0])))) * (*((int16_t *)((arguments[1]))));
                instance->velocity += dv;
                
                int16_t ds = instance->velocity * (*((int16_t *)((arguments[1]))));
                instance->distance_flown += ds;
                
                instance->travel_time += (*((int16_t *)((arguments[1]))));
                
                SpaceshipMain_printMetric("Current Spaceship velocity: ", (instance->velocity), "m/s");
              }

              /* 
               * switch state
               */

              instance->__currentState = SpaceshipMain_Spaceship__states__Spaceship_flying__state;
              __isEpsilonEvent &= true;
              break;
            }

            break;
          }
          case SpaceshipMain_Spaceship__inevents__Spaceship_land__event: {
            if (!SpaceshipMain_canLand(instance->velocity)) 
            {
              {
                /* 
                 * transition actions
                 */

                instance->velocity = 0;
                printf("Spaceship has crashed\n");
                fflush(stdout);
              }

              /* 
               * switch state
               */

              instance->__currentState = SpaceshipMain_Spaceship__states__Spaceship_crashed__state;
              __isEpsilonEvent &= true;
              break;
            }

            if (SpaceshipMain_canLand(instance->velocity)) 
            {
              {
                /* 
                 * transition actions
                 */

                instance->velocity = 0;
                printf("Spaceship has landed\n");
                fflush(stdout);
                
                SpaceshipMain_printMetric("Total distance flown: ", (instance->distance_flown), "m");
                SpaceshipMain_printMetric("Total travel time: ", (instance->travel_time), "s");
              }

              /* 
               * switch state
               */

              instance->__currentState = SpaceshipMain_Spaceship__states__Spaceship_parked_engine_on__state;
              __isEpsilonEvent &= true;
              break;
            }

            break;
          }
        }

        break;
      }
      case SpaceshipMain_Spaceship__states__Spaceship_crashed__state: {
        switch (event)
        {
        }

        break;
      }
    }

    
  }
 while (!__isEpsilonEvent);
}


static bool SpaceshipMain_canLand(int16_t velocity) 
{
  if (velocity >= 0 && velocity < 100) 
  {
    return true;
  }
  else
  {
    return false;
  }
}


static void SpaceshipMain_printMetric(char *description, int16_t metric, char *unit) 
{
  
  printf(description);
  char x[20];
  /* 
   * Bei Conversion TypeSizeConfiguration beachten!
   */

  sprintf(x, "%hi", metric);
  printf(x);
  printf(unit);
  printf("\n");
  fflush(stdout);
}


int32_t main(int32_t argc, char *(argv[])) 
{
  SpaceshipMain_Spaceship__data_t xwing;
  SpaceshipMain_Spaceship__init(&xwing);
  
  int16_t dt = 10;
  int16_t a = 10;
  
  {
    SpaceshipMain_Spaceship__execute(&xwing, SpaceshipMain_Spaceship__inevents__Spaceship_power_on__event, NULL);
  }

  {
    SpaceshipMain_Spaceship__execute(&xwing, SpaceshipMain_Spaceship__inevents__Spaceship_liftoff__event, NULL);
  }

  
  {
    int16_t const  _event_arg_a = 7 * a;
    int16_t const  _event_arg_t = 6 * dt;
    void *(___args[2]) = {
      &_event_arg_a,      &_event_arg_t    };
    SpaceshipMain_Spaceship__execute(&xwing, SpaceshipMain_Spaceship__inevents__Spaceship_accelerate__event, ___args);
  }

  {
    int16_t const  _event_arg_a = 5 * a;
    int16_t const  _event_arg_t = 20 * dt;
    void *(___args[2]) = {
      &_event_arg_a,      &_event_arg_t    };
    SpaceshipMain_Spaceship__execute(&xwing, SpaceshipMain_Spaceship__inevents__Spaceship_accelerate__event, ___args);
  }

  {
    int16_t const  _event_arg_a = -35 * a;
    int16_t const  _event_arg_t = 4 * dt;
    void *(___args[2]) = {
      &_event_arg_a,      &_event_arg_t    };
    SpaceshipMain_Spaceship__execute(&xwing, SpaceshipMain_Spaceship__inevents__Spaceship_accelerate__event, ___args);
  }

  {
    int16_t const  _event_arg_a = -1 * a;
    int16_t const  _event_arg_t = 2 * dt;
    void *(___args[2]) = {
      &_event_arg_a,      &_event_arg_t    };
    SpaceshipMain_Spaceship__execute(&xwing, SpaceshipMain_Spaceship__inevents__Spaceship_accelerate__event, ___args);
  }

  
  /* 
   * comment this out, else the spaceship will crash
   */

  
  {
    SpaceshipMain_Spaceship__execute(&xwing, SpaceshipMain_Spaceship__inevents__Spaceship_land__event, NULL);
  }

  {
    SpaceshipMain_Spaceship__execute(&xwing, SpaceshipMain_Spaceship__inevents__Spaceship_power_off__event, NULL);
  }

  
  return 0;
}


